#!/bin/bash

# check integrity of data

if [[ -z $1 ]] ; then
  echo "Usage: `basename $0` DPR_DATA_DIRECTORY DPR_LEGACY_DIRECTORY LOG_DIRECTORY"
  exit 0
fi

DPR_DATA=$1
DPR_LEGACY=$2
LOG_DIR=$3

TODAY=$(date +%F)
LOG_FILE="${LOG_DIR}/${TODAY}_integrity_errors.log"

EXITCODE=0

# check if files are missing or modified
DIFF_MYANMAR=$(git diff --compact-summary "${DPR_DATA}/data/DPRMyanmar" "${DPR_LEGACY}/DPRMyanmar/content/xml")

if [[ ! $? -eq "0" ]]; then
  echo "check for myanmar data failed"
  EXITCODE=1
fi

if [[ ! -z $DIFF_MYANMAR ]]; then
  echo $DIFF_MYANMAR >> $LOG_FILE
  EXITCODE=1
fi

DIFF_THAI=$(git diff --compact-summary "${DPR_DATA}/data/DPRThai" "${DPR_LEGACY}/DPRThai/content/xml")

if [[ ! $? -eq "0" ]]; then
  echo "check for thai data failed"
  EXITCODE=1
fi

if [[ ! -z $DIFF_THAI ]]; then
  echo $DIFF_THAI >> $LOG_FILE
  EXITCODE=1
fi

DIFF_ETC=$(git diff --compact-summary "${DPR_DATA}/data/etc" "${DPR_LEGACY}/digitalpalireader/content/etc")

if [[ ! $? -eq "0" ]]; then
  echo "check for etc data failed"
  EXITCODE=1
fi

if [[ ! -z $DIFF_ETC ]]; then
  echo $DIFF_ETC >> $LOG_FILE
  EXITCODE=1
fi

DIFF_SANSKRIT=$(git diff --compact-summary "${DPR_DATA}/data/sanskrit" "${DPR_LEGACY}/sanskrit/content/xml")

if [[ ! $? -eq "0" ]]; then
  echo "check for sanskrit data failed"
  EXITCODE=1
fi

if [[ ! -z $DIFF_SANSKRIT ]]; then
  echo $DIFF_SANSKRIT >> $LOG_FILE
  EXITCODE=1
fi

if [[ $EXITCODE -eq "0" ]]; then
  echo "=== check passed ==="
else
  echo "=== check failed (see ${LOG_FILE}) ==="
fi

exit $EXITCODE;
